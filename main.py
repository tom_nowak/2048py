import argparse
import numpy as np
from game import Game
import ai

def main():
    parser = argparse.ArgumentParser(description="Play 2048 by yourself or wath AI play the game!")
    parser.add_argument("--player", dest="player", default="human",
                        help="player (human, AI options)",
                        choices=["human", "random_ai", "cma_es_train", "cma_es"])
    parser.add_argument("--size", dest="size", type=int, default=4, help="board size (default 4)")
    parser.add_argument("--number", dest="number", type=int, default=2048, help="number winning game (default 2048)")
    parser.add_argument("--time", dest="time", type=int, default=0,
                        help="time limit in seconds (used only in non-interactive mode)")
    parser.add_argument("--stats", dest="stats", action="store_const", const=True, default=False,
                        help="generate statistics of many games during given time (works only for AI players)")
    parser.add_argument("--file", dest="file", default="",
                        help="filename (input in case of cma_es, output in case of cma_es_train, unused otherwise)")

    game = make_game(parser.parse_args())
    game.play()


def make_game(parser_args):  # -> Playable (object with method play)
    if parser_args.player == "human":
        return Game(parser_args.size, parser_args.number)
    if parser_args.player == "cma_es_train":
        return ai.CMAESTrainer(parser_args.time, parser_args.file, parser_args.size)
    game = None
    if parser_args.player == "random_ai":
        game = ai.RandomAIGame(parser_args.size, parser_args.number)
    elif parser_args.player == "cma_es":
        game = ai.CMAESGame(parser_args.size, load_coefficients(parser_args.file), parser_args.number)
    if parser_args.stats:
        return ai.StatisticsGenerator(parser_args.time, game)
    return game


def load_coefficients(filename: str) -> np.array:
    if filename:
        return np.loadtxt(filename)
    # temporary for testing:
    return [
        6.46032865, 4.20942805, 0.97991147, -3.86667455, 6.77585069, 5.29117223, 1.85303895, -3.00365129,
        3.73491057, 3.58952417, -1.97823455, -1.79334163, 2.51137499, -6.93993863, -3.82667326, -10.22422469,
        1.63654565, -0.54674296, -0.43051448, -1.99994061, -0.13209661, -1.63747671, -2.5150689, -1.78720987
    ]


if __name__ == "__main__":
    main()
