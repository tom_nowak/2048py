import unittest
import numpy as np
import ai
import game

class TestAi(unittest.TestCase):
    def test_rate_board_simple(self):
        board = game.Board(3)
        board.spawn_number_at_given_position((0, 0), 2)
        board.spawn_number_at_given_position((1, 1), 4)
        coefficients = np.ones(15,)
        coefficients[0] = 0.5
        self.assertEqual(ai.rate_board_simple(board, coefficients), 1 + 4 + 2 + 4 + 2 + 4)
