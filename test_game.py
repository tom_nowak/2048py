import unittest
import numpy as np
import game

# pylint: disable=protected-access,too-many-public-methods
class TestBoard(unittest.TestCase):
    def make_board(self, size) -> game.Board:
        return game.Board(size)

    def test_board_is_constructed_empty(self):
        board = self.make_board(4)
        self.assertEqual(board.data[0, 0], 0)
        self.assertEqual(board.data[3, 3], 0)

    def test_cannot_modify_data(self):
        board = self.make_board(3)
        with self.assertRaises(Exception):
            board.data = 1

    def test_to_string(self):
        board = self.make_board(4)
        expected_str = (
            "0    0    0    0   \n"
            "0    0    0    0   \n"
            "0    0    0    0   \n"
            "0    0    0    0   "
        )
        self.assertEqual(str(board), expected_str)

    def test_size(self):
        board = self.make_board(3)
        self.assertEqual(board.size, 3)

    def test_empty_fields(self):
        board = self.make_board(4)
        self.assertEqual(board.empty_fields_count, 16)
        self.assertEqual(len(set(board.empty_fields_indices)), 16)
        board.spawn_number_at_random_position()
        self.assertEqual(board.empty_fields_count, 15)
        self.assertEqual(len(set(board.empty_fields_indices)), 15)

    def test_reset(self):
        board = self.make_board(3)
        board.spawn_number_at_given_position((1, 1), 2)
        board.move("right")
        board.reset()
        self.assertEqual(board.empty_fields_count, 9)
        self.assertTrue(np.array_equal(board.data, np.zeros_like(board.data)))
        self.assertEqual(board.highest_number, 0)
        self.assertEqual(board.last_move_succeeded, False)

    def test_copy_from(self):
        board1 = self.make_board(3)
        board2 = self.make_board(3)
        board1.spawn_number_at_given_position((1, 1), 2)
        board1.move("left")
        board2.copy_from(board1)
        self.assertEqual(board2.data[(1, 0)], 2)
        self.assertEqual(board2.empty_fields_count, 8)
        self.assertEqual(board2.highest_number, 2)
        self.assertTrue(board2.last_move_succeeded)

    def test_spawn_number_on_full_board(self):
        board = self.make_board(3)
        for _ in range(10):
            board.spawn_number_at_random_position()
        self.assertEqual(board.empty_fields_count, 0)
        self.assertEqual(len(set(board.empty_fields_indices)), 0)

    def test_spawn_number_at_given_position(self):
        board = self.make_board(3)
        board.spawn_number_at_given_position((0, 1), 4)
        with self.assertRaises(ValueError):
            board.spawn_number_at_given_position((0, 1), 4)
        self.assertEqual(board.data[(0, 1)], 4)
        self.assertEqual(board.empty_fields_count, 8)

    def test_move_one_number(self):
        board = self.make_board(4)
        board.spawn_number_at_given_position((0, 0), 2)
        board._move_one_number((0, 0), (0, 3))
        self.assertEqual(board.data[(0, 0)], 0)
        self.assertEqual(board.data[(0, 3)], 2)
        self.assertEqual(board.empty_fields_count, 15)
        with self.assertRaises(ValueError):
            board._move_one_number((0, 0), (3, 0))

    def test_merge_one_number(self):
        board = self.make_board(4)
        board.spawn_number_at_given_position((0, 2), 2)
        board.spawn_number_at_given_position((0, 3), 2)
        self.assertEqual(board.empty_fields_count, 14)
        board._merge_one_number((0, 2), (0, 3))
        self.assertEqual(board.data[(0, 2)], 0)
        self.assertEqual(board.data[(0, 3)], 4)
        self.assertEqual(board.empty_fields_count, 15)
        with self.assertRaises(ValueError):
            board._merge_one_number((0, 2), (0, 3))

    def test_first_position_with_predicate(self):
        board = self.make_board(4)
        board.spawn_number_at_given_position((0, 0), 2)
        board.spawn_number_at_given_position((3, 0), 4)
        self.assertEqual(board._first_position_with_predicate((0, 0), (1, 0), lambda pos: board.data[pos] == 2), (0, 0))
        self.assertEqual(board._first_position_with_predicate((0, 0), (1, 0), lambda pos: board.data[pos] == 4), (3, 0))
        incorrect_position = board._first_position_with_predicate((0, 0), (1, 0), lambda pos: board.data[pos] == 8)
        self.assertFalse(board.is_position_correct(incorrect_position))

    def test_move_right(self):
        board = self.make_board(3)
        board.spawn_number_at_given_position((1, 1), 2)
        board.move("right")
        expected_str = (
            "0    0    0   \n"
            "0    0    2   \n"
            "0    0    0   "
        )
        self.assertEqual(str(board), expected_str)

    def test_move_left(self):
        board = self.make_board(3)
        board.spawn_number_at_given_position((1, 1), 2)
        board.move("left")
        expected_str = (
            "0    0    0   \n"
            "2    0    0   \n"
            "0    0    0   "
        )
        self.assertEqual(str(board), expected_str)

    def test_move_down(self):
        board = self.make_board(3)
        board.spawn_number_at_given_position((1, 1), 2)
        board.move("down")
        expected_str = (
            "0    0    0   \n"
            "0    0    0   \n"
            "0    2    0   "
        )
        self.assertEqual(str(board), expected_str)

    def test_move_up(self):
        board = self.make_board(3)
        board.spawn_number_at_given_position((1, 1), 2)
        board.move("up")
        expected_str = (
            "0    2    0   \n"
            "0    0    0   \n"
            "0    0    0   "
        )
        self.assertEqual(str(board), expected_str)

    def test_move_empty(self):
        board = self.make_board(4)
        board.move("left")
        expected_str = (
            "0    0    0    0   \n"
            "0    0    0    0   \n"
            "0    0    0    0   \n"
            "0    0    0    0   "
        )
        self.assertEqual(str(board), expected_str)
        self.assertEqual(board.empty_fields_count, 16)

    def test_move_complex_1(self):
        board = self.make_board(4)
        board.spawn_number_at_given_position((0, 2), 2)
        board.spawn_number_at_given_position((0, 3), 2)
        board.spawn_number_at_given_position((1, 1), 2)
        board.spawn_number_at_given_position((1, 3), 2)
        board.spawn_number_at_given_position((2, 0), 16)
        board.spawn_number_at_given_position((2, 1), 8)
        board.spawn_number_at_given_position((2, 2), 4)
        board.spawn_number_at_given_position((2, 3), 2)
        board.spawn_number_at_given_position((3, 1), 8)
        board.spawn_number_at_given_position((3, 2), 2)
        board.spawn_number_at_given_position((3, 3), 2)
        board.move("left")
        expected_str = (
            "4    0    0    0   \n"
            "4    0    0    0   \n"
            "16   8    4    2   \n"
            "8    4    0    0   "
        )
        self.assertEqual(str(board), expected_str)
        self.assertEqual(board.empty_fields_count, 8)

    def test_move_complex_2(self):
        board = self.make_board(4)
        board.spawn_number_at_given_position((0, 0), 2)
        board.spawn_number_at_given_position((1, 0), 2)
        board.spawn_number_at_given_position((2, 0), 2)
        board.spawn_number_at_given_position((3, 0), 2)
        board.spawn_number_at_given_position((1, 1), 2)
        board.spawn_number_at_given_position((0, 3), 4)
        board.spawn_number_at_given_position((3, 3), 4)
        board.move("up")
        expected_str = (
            "4    2    0    8   \n"
            "4    0    0    0   \n"
            "0    0    0    0   \n"
            "0    0    0    0   "
        )
        self.assertEqual(str(board), expected_str)
        self.assertEqual(board.empty_fields_count, 12)

    def test_highest_number(self):
        board = self.make_board(3)
        self.assertEqual(board.highest_number, 0)
        board.spawn_number_at_given_position((0, 0), 2)
        board.spawn_number_at_given_position((0, 1), 2)
        self.assertEqual(board.highest_number, 2)
        board.move("right")
        self.assertEqual(board.highest_number, 4)

    def test_last_move_succeeded(self):
        board = self.make_board(3)
        board.spawn_number_at_given_position((1, 0), 2)
        self.assertEqual(board.last_move_succeeded, False)
        board.move("left")
        self.assertEqual(board.last_move_succeeded, False)
        board.move("down")
        self.assertEqual(board.last_move_succeeded, True)


class TestablePredictiveBoard(game.PredictiveBoard):
    def move(self, direction: str):
        super().simulate_possible_moves()
        super().move(direction)
        if self.last_move_succeeded and direction not in self.possible_moves:
            raise ValueError(f"{direction} not in possible moves {self.possible_moves}")


class TestPredictiveBoard(TestBoard):
    def make_board(self, size) -> game.Board:
        return TestablePredictiveBoard(size)
