import random
import time
import cma
import numpy as np
import game

class RandomAIGame(game.Game):
    def perform_move(self) -> bool:
        self.board.simulate_possible_moves()
        if self.board.possible_moves == []:
            return False
        move_direction = self.board.possible_moves[random.randint(0, len(self.board.possible_moves) - 1)]
        if self.verbose:
            print(f"random_ai chooses {move_direction}")
        self.board.move(move_direction)
        return True


class StatisticsGenerator:
    def __init__(self, time_limit: float, game_instance: game.Game):
        self.time_limit = time_limit
        self.game = game_instance
        self.game.verbose = False

    def play(self):
        start_time = time.time()
        games = 0
        wins = 0
        while True:
            games += 1
            wins += int(self.game.play_one_game())
            if time.time() - start_time > self.time_limit:
                break
        print(f"games: {games}, win rate: {wins/games}")


def rate_board_simple(board: game.Board, coefficients: np.array) -> float:
    size = board.size ** 2
    retval = np.dot(np.reshape(board.data, (size,)), coefficients[:size])
    j = size
    for i in range(board.size):
        retval += np.max(board.data[:, i]) * coefficients[j]
        j += 1
    for i in range(board.size):
        retval += np.max(board.data[i, :]) * coefficients[j]
        j += 1
    return retval


class CMAESGame(game.Game):
    def __init__(self, board_size: int, coefficients: np.array, number_to_be_reached=2048, rate_board_function=rate_board_simple):
        self.coefficients = coefficients
        self.rate_board_function = rate_board_function
        super().__init__(board_size, number_to_be_reached)

    def perform_move(self) -> bool:
        self.board.simulate_possible_moves()
        if self.board.possible_moves == []:
            return False
        move_direction = self.choose_move_direction()
        if self.verbose:
            print(f"CMA-ES-based heuristics chooses {move_direction}")
        self.board.move(move_direction)
        return True

    def choose_move_direction(self) -> str:
        results = ((direction, self.rate_board_function(self.board.possible_moves_results[direction], self.coefficients))
                   for direction in self.board.possible_moves)
        return max(results, key=lambda result: result[1])[0]


class CMAESTrainer(CMAESGame):
    def __init__(self, time_limit: float, output_filename: str, board_size: int):
        self.time_limit = time_limit
        self.output_filename = output_filename
        self.highest_number = 0
        self.games = 0
        # absurd number to be reached on purpose - game will always last until loss
        super().__init__(board_size, np.ones(board_size ** 2 + board_size * 2), 999999999999)
        self.verbose = False

    def play(self):
        start_time = time.time()
        cma_es = cma.CMAEvolutionStrategy(self.coefficients, 1.0)
        iteration = 0
        while time.time() - start_time <= self.time_limit:
            cma_es.tell(*cma_es.ask_and_eval(self))
            iteration = iteration + 1
            if iteration % 10 == 1:
                print(f"iteration {iteration}: {cma_es.result.fbest}")
        print(f"CMA-ES result: {cma_es.result.xbest}\ngoal function: {cma_es.result.fbest}\n" +
              f"highest number: {self.highest_number}\ngames: {self.games}")
        if self.output_filename:
            np.savetxt(self.output_filename, cma_es.result.xbest)

    # goal function to be minimized
    def __call__(self, coefficients: np.array):
        np.copyto(self.coefficients, coefficients)
        result = 0.0
        for _ in range(3):  # 3 games - arbitrary choice - to reduce randomness
            self.play_one_game()
            self.highest_number = max(self.highest_number, self.board.highest_number)
            self.games += 1
            result -= (np.sum(self.board.data) + np.max(self.board.data))
        return result
