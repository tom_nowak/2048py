#### Setup (Linux)
```
virtualenv -p python3.7 venv
. venv/bin/activate
pip install -r requirements.txt
```

#### Running main program
```
python main.py -h  # will print command line options
```

#### Running linter
```
pylint *.py
```

#### Running tests
```
python -m unittest
```
