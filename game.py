from __future__ import annotations
from typing import Iterable
import random
import numpy as np

class Board:
    def __init__(self, size):
        self._data = np.zeros((size, size), dtype=np.uint16)
        self._empty_fields_count = len(list(self.empty_fields_indices))
        self._highest_number = 0
        self._last_move_succeeded = False

    def __str__(self) -> str:
        return '\n'.join(' '.join("{:<4d}".format(number) for number in row) for row in self._data)

    @staticmethod
    def advance_position(position: (int, int), direction: (int, int), distance=1) -> (int, int):
        return (position[0] + distance * direction[0], position[1] + distance * direction[1])

    @property
    def data(self) -> np.array:
        return self._data

    @property
    def size(self) -> int:
        return self._data.shape[0]

    @property
    def empty_fields_count(self) -> int:
        return self._empty_fields_count

    @property
    def empty_fields_indices(self) -> Iterable[(int, int)]:
        for index, number in np.ndenumerate(self._data):
            if number == 0:
                yield index
    @property
    def highest_number(self):
        return self._highest_number

    @property
    def last_move_succeeded(self):
        return self._last_move_succeeded

    def reset(self):
        self._data.fill(0)
        self._empty_fields_count = self.size * self.size
        self._highest_number = 0
        self._last_move_succeeded = False

    def copy_from(self, board: Board):
        np.copyto(self._data, board.data)
        self._empty_fields_count = board.empty_fields_count
        self._highest_number = board.highest_number
        self._last_move_succeeded = board.last_move_succeeded

    def spawn_number_at_random_position(self, number=2):
        if self._empty_fields_count == 0:
            return
        index_to_be_changed = random.randint(0, self._empty_fields_count - 1)
        for index, board_position in enumerate(self.empty_fields_indices):
            if index == index_to_be_changed:
                self.spawn_number_at_given_position(board_position, number)
                return
        raise RuntimeError(
            f"this code should be unreachable!\n"
            f"index_to_be_changed: {index_to_be_changed}\n"
            f"_empty_fields_count: {self._empty_fields_count}\n"
            f"len(empty_fields_indices): {len(list(self.empty_fields_indices))}"
        )

    def spawn_number_at_given_position(self, position: (int, int), number=2):
        if self._data[position] != 0:
            raise ValueError(f"cannot put new number on {position} - position not empty")
        self._data[position] = number
        self._empty_fields_count -= 1
        self._highest_number = max(self._highest_number, number)

    def move_right(self):
        self._move_generic((0, self.size - 1), (0, -1), (1, 0))

    def move_left(self):
        self._move_generic((0, 0), (0, 1), (1, 0))

    def move_down(self):
        self._move_generic((self.size - 1, 0), (-1, 0), (0, 1))

    def move_up(self):
        self._move_generic((0, 0), (1, 0), (0, 1))

    def move(self, direction: str):
        getattr(self, f"move_{direction}")()

    def is_position_correct(self, position: (int, int)) -> bool:
        return position[0] >= 0 and position[1] >= 0 and position[0] < self.size and position[1] < self.size

    def _move_one_number(self, from_position: (int, int), to_position: (int, int)):
        if self._data[from_position] == 0 or self._data[to_position] != 0:
            raise ValueError(f"incorrect _move_one_number arguments {from_position}, {to_position}")
        self._data[to_position] = self._data[from_position]
        self._data[from_position] = 0
        self._last_move_succeeded = True

    def _merge_one_number(self, from_position: (int, int), to_position: (int, int)):
        if self._data[from_position] != self._data[to_position] or self._data[from_position] == 0:
            raise ValueError(f"incorrect _merge_one_number arguments {from_position}, {to_position}")
        self._data[to_position] *= 2
        self._data[from_position] = 0
        self._empty_fields_count += 1
        self._last_move_succeeded = True
        self._highest_number = max(self._highest_number, self._data[to_position])

    def _first_position_with_predicate(self, start_position: (int, int), direction: (int, int), predicate=lambda _: True) -> (int, int):
        position = start_position
        while self.is_position_correct(position):
            if predicate(position):
                return position
            position = self.advance_position(position, direction)
        return position

    def _move_line(self, line_end: (int, int), direction: (int, int)):
        destination = line_end
        source = self._first_position_with_predicate(destination, direction, lambda pos: self._data[pos] != 0)
        while self.is_position_correct(source):
            if self._data[destination] == 0:
                self._move_one_number(source, destination)
                source = self._first_position_with_predicate(source, direction, lambda pos: self._data[pos] != 0)
            elif source != destination:
                if self._data[source] == self._data[destination]:
                    self._merge_one_number(source, destination)
                    source = self._first_position_with_predicate(source, direction, lambda pos: self._data[pos] != 0)
                destination = self.advance_position(destination, direction)
            else:
                source = self.advance_position(source, direction)
                source = self._first_position_with_predicate(source, direction, lambda pos: self._data[pos] != 0)

    def _move_generic(self, start_position: (int, int), direction_in_line: (int, int), line_change_direction: (int, int)):
        self._last_move_succeeded = False
        position = start_position
        while self.is_position_correct(position):
            self._move_line(position, direction_in_line)
            position = self.advance_position(position, line_change_direction)


class PredictiveBoard(Board):
    def __init__(self, size):
        super().__init__(size)
        self.possible_moves_results = {
            "right": Board(size),
            "left": Board(size),
            "down": Board(size),
            "up": Board(size),
        }
        self.possible_moves = []

    # this function works correctly only if board has not been changed since last simulate_possible_moves call
    def move(self, direction: str):
        self.copy_from(self.possible_moves_results[direction])

    def simulate_possible_moves(self):
        self.possible_moves.clear()
        for direction in ["right", "left", "down", "up"]:
            self.possible_moves_results[direction].copy_from(self)
            self.possible_moves_results[direction].move(direction)
            if self.possible_moves_results[direction].last_move_succeeded:
                self.possible_moves.append(direction)


# functions which can be overloaded in (for example) non-interactive game played by AI
# initialize_board
# spawn_new_number
# perform_move
# print_before_move
# print_after_move
# play (many games can be played to obtain statistics, etc)
class Game:
    def __init__(self, board_size, number_to_be_reached=2048):
        self.board = PredictiveBoard(board_size)
        self.number_to_be_reached = number_to_be_reached
        self.verbose = True

    def initialize_board(self):
        self.board.spawn_number_at_random_position()
        self.board.spawn_number_at_random_position()

    def spawn_new_number(self):
        number = 4 if random.randint(0, 4) == 0 else 2
        self.board.spawn_number_at_random_position(number)

    def perform_move(self) -> bool:
        self.board.simulate_possible_moves()
        if self.board.possible_moves == []:
            return False
        move_direction = ""
        while move_direction not in self.board.possible_moves:
            move_direction = input(f"choose move direction from {self.board.possible_moves} ")
        self.board.move(move_direction)
        return True

    def print_before_move(self):
        if self.verbose:
            print(self.board)

    def print_after_move(self):
        if not self.verbose:
            return
        print(self.board)
        if not self.board.last_move_succeeded:  # sanity check, it should never happen
            print("warning - last move failed!")
        if self.board.highest_number >= self.number_to_be_reached:
            return
        if self.board.empty_fields_count > 0:
            print("spawning new number...")
        else:
            print("not spawning new number (board full)")  # probably unreachable code, but there is nothing bad in leaving it as is

    def play(self) -> bool:
        if self.play_one_game():
            print("victory!")
        else:
            print("loss!")

    # returns bool - whether player won or lost
    def play_one_game(self) -> bool:
        self.board.reset()
        self.initialize_board()
        while self.board.highest_number < self.number_to_be_reached:
            self.print_before_move()
            if not self.perform_move():
                return False
            self.print_after_move()
            self.spawn_new_number()
        return True
